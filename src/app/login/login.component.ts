import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
  isError: boolean = false;
  errorMessage: string;

  constructor(private AuthService: AuthService, private Router: Router) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.AuthService.login(this.email, this.password).then(res => {
      console.log(res);
      this.Router.navigateByUrl('/welcome');
    }).catch(
      err => {
        console.log(err);
        this.isError = true;
        this.errorMessage = err.message;
      }
    )
  }

}
