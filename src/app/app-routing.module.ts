import { StudentsComponent } from './students/students.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { LambdaComponent } from './lambda/lambda.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'login', component: LoginComponent  },
  { path: 'register', component: RegisterComponent },
  { path: 'lambda', component: LambdaComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'students', component: StudentsComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
