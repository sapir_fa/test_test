import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';



@Injectable({
  providedIn: 'root'
})
export class LambdaService {
   
  private url : string = "https://dj3rznmt9i.execute-api.us-east-1.amazonaws.com/beta";
  constructor(private http : HttpClient) { }


  classify(math,psycho,payment){
    let temp = {data:{math,psycho,payment}}
    let body =JSON.stringify(temp)
    console.log(body)
    return this.http.post(this.url,body)
       
    
  }
}



