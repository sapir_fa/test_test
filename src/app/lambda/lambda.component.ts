import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { StudentsService } from './../students.service';
import { LambdaService } from './../lambda.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lambda',
  templateUrl: './lambda.component.html',
  styleUrls: ['./lambda.component.css']
})
export class LambdaComponent implements OnInit {

  constructor(public LambdaService: LambdaService, public StudentsService: StudentsService, public AuthService: AuthService,
    public Router : Router) { }
 
  predict = undefined
  MathGrade : number;
  PsychoGrade: number;
  payment;
  probability:number
  userId;



  ngOnInit(): void {
    this.AuthService.getUser().subscribe(
      user => {
        this.userId = user.uid;
      }
    )
  }

  onSubmit(){
    console.log(this.MathGrade,this.PsychoGrade,this.payment)
   this.LambdaService.classify(this.MathGrade,this.PsychoGrade,this.payment).subscribe(
     (res:any)=>{console.log(res)
      if(res.body >= 0.5){
        this.probability = res.body
         this.predict = "Falls out"
      }
      if(res.body < 0.5){
        this.probability = res.body
        this.predict = "Does not fall out"
      }
    }

   )
  }

  cancel(){
    this.predict = undefined
  }

  save(){
     this.StudentsService.addStudent(this.userId,this.MathGrade,this.PsychoGrade,this.payment,this.predict,this.probability)
     this.Router.navigateByUrl('/students')
  }

}
