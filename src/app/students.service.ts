import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  StudentsCollection: AngularFirestoreCollection;
  UserCollection: AngularFirestoreCollection =this.db.collection('users');

  constructor(private db: AngularFirestore) { }


  addStudent(userId:string, mathgrade, psychograde,payments, predict, probability){
    const student = {"mathgrade": mathgrade, "psychograde": psychograde, "payments" :payments, "fallsout" : predict, "predict" : probability};
    this.UserCollection.doc(userId).collection('students').add(student);
    }


    deleteStudent(Userid:string,id:string){
      this.db.doc(`users/${Userid}/students/${id}`).delete();
    }



    public getStudents(userId) {
      this.StudentsCollection = this.db.collection(`users/${userId}/students`,
      ref=> ref.limit(1))
      return this.StudentsCollection.snapshotChanges()
    }
  
    public getPrevstudent(userId, startAt) {
      this.StudentsCollection = this.db.collection(`users/${userId}/students`,
      ref=> ref.limit(1).startAt(startAt));
      return this.StudentsCollection.snapshotChanges()
    }
  
    public nextPage(userId,startAfter): Observable<any[]>{
      this.StudentsCollection = this.db.collection(`users/${userId}/students`, 
      ref => ref.limit(1).startAfter(startAfter))    
      return this.StudentsCollection.snapshotChanges();
    }
    
    // addUser(userid){
    //   this.UserCollection.doc(userid)
    //   }
}
