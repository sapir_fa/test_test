import { StudentsService } from './../students.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  userId;
  students$
  students

  firstDocumentArrived: any;

  //Save last document in snapshot of items received
  lastDocumentArrived: any;

  //Keep the array of first document of previous pages
  prev_strt_at: any[] = [];

  push_prev_startAt(prev_first_doc) {
    this.prev_strt_at.push(prev_first_doc);
  }

  remove_last_from_start_at() {
    this.prev_strt_at.splice(this.prev_strt_at.length - 1, 1);
  }
 
  get_prev_startAt(){
    return this.prev_strt_at[this.prev_strt_at.length - 1];
}
  // lastDocumentArrived

  constructor(public AuthService: AuthService, public StudentsService: StudentsService) { }

  ngOnInit(): void {
   
    this.AuthService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.students$ = this.StudentsService.getStudents(this.userId);
        this.students$.subscribe(
          docs => {
            this.lastDocumentArrived = docs[docs.length-1].payload.doc;
            this.firstDocumentArrived = docs[0].payload.doc;
            this.push_prev_startAt(this.firstDocumentArrived);  
            this.students = [];
            for (let document of docs) {
              const student = document.payload.doc.data();
              student.id = document.payload.doc.id;
              this.students.push(student);
            }
            console.log(this.students)
          }
        )
      }
    )
  }


  onDelete(studentid){
    this.StudentsService.deleteStudent(this.userId,studentid)
  }

  previusPage(){
    this.remove_last_from_start_at()
    this.students$ = this.StudentsService.getPrevstudent(this.userId, this.get_prev_startAt());
    this.students$.subscribe(
      docs => {
        this.lastDocumentArrived = docs[docs.length - 1].payload.doc;
        this.firstDocumentArrived = docs[0].payload.doc;
        this.students= [];
        for (let document of docs) {
          const student = document.payload.doc.data();
          student.id = document.payload.doc.id;
          this.students.push(student);
        }
      }
    )
  }


  nextPage() {
    console.log("nexttt")
    this.students$ = this.StudentsService.nextPage(this.userId, this.lastDocumentArrived);
    this.students$.subscribe(
      docs => {
        this.lastDocumentArrived = docs[docs.length - 1].payload.doc;
        this.firstDocumentArrived = docs[0].payload.doc;
        this.push_prev_startAt(this.firstDocumentArrived);
        this.students = [];
        for (let document of docs) {
          const student =document.payload.doc.data();
          student.id = document.payload.doc.id;
          this.students.push(student);
        }
        console.log(this.students)
      }
    )
  }
  




}
