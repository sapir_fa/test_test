import { StudentsService } from './../students.service';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email: string;
  password: string;
  isError: boolean = false;
  errorMessage:string;
  userId

  constructor(private Auth: AuthService, private Router: Router, private StudentsService:StudentsService) { }

  ngOnInit(): void {
  }

  onRegister() {
    this.Auth.register(this.email, this.password).then(
      res => {
        console.log('Succesful login');
        // this.userId = this.Auth.getUser
        // console.log(this.afAuth.createUserWithEmailAndPassword);
        // this.StudentsService.addUser(this.userId)
        this.Router.navigateByUrl('/welcome');
      }).catch(
        err => {
          console.log(err);
          this.isError = true;
          this.errorMessage = err.message;
        }
      )
  }

}
