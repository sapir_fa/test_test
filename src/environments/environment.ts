// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBUU0hjaMkHR6_h8yWc2a4AdffgYFUukOg",
    authDomain: "sapir-f-test.firebaseapp.com",
    projectId: "sapir-f-test",
    storageBucket: "sapir-f-test.appspot.com",
    messagingSenderId: "728250917586",
    appId: "1:728250917586:web:2b1180832396e22787030b"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
